# Sum the Largest 2 Numbers in an Array

This package accepts any array of numbers and sums the highest 2 values.

## Installation

Once you've cloned the repository, just run `yarn`.

## Usage

Usage in TypeScript or JavaScript is essentially the same, though your import/require syntax may vary.

### TypeScript

```typescript
import * as sumTwoLargest from 'sum-two-largest-numbers';

sumTwoLargest([5, 3, 10, 8]);

>> 18
```

### Javascript

```javascript
sumTwoLargest = require('sum-two-largest-numbers');

sumTwoLargest([5, 3, 10, 8]);

>> 18
```

## Testing

To run the tests, just run `yarn tests` or `npm run tests`.

## Contributing

If you feel like suggesting improvements, I'd happily accept and review pull requests - Just make sure the linter is happy and tests are passing.
