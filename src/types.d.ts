type Tuple<A, B = A> = [A, B];

type ArrayOfTwoOrMore<T> = [T, T, ...T[]];
